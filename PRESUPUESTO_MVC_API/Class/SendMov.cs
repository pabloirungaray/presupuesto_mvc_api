﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PRESUPUESTO_MVC_API.Class
{
    public class SendMov
    {
        private int anual;
        private int idu;

        public int Anual { get => anual; set => anual = value; }
        public int Idu { get => idu; set => idu = value; }
    }
}