﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PRESUPUESTO_MVC_API.Class
{
    public class ListaMovimiento
    {
        //P.ID_PRESUPUESTO, P.MONTO, P.ANUAL, P.MES, M.DESCRIPCION,M.MONTO OPERAR, M.ID_TIPOMOVIMIENTO, M.FECHA
        private string id_presupuesto;
        private decimal monto;
        private string anual;
        private string mes;
        private string descripcion;
        private decimal operar;
        private int tipomovimiento;
        private string fecha;
        private int idu;

        public string Id_presupuesto { get => id_presupuesto; set => id_presupuesto = value; }
        public decimal Monto { get => monto; set => monto = value; }
        public string Anual { get => anual; set => anual = value; }
        public string Mes { get => mes; set => mes = value; }
        public string Descripcion { get => descripcion; set => descripcion = value; }
        public decimal Operar { get => operar; set => operar = value; }
        public int Tipomovimiento { get => tipomovimiento; set => tipomovimiento = value; }
        public string Fecha { get => fecha; set => fecha = value; }
        public int Idu { get => idu; set => idu = value; }
    }
}