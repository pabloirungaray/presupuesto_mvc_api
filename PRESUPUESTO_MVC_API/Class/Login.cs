﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PRESUPUESTO_MVC_API.Class
{
    public class Login
    {
        private string user;
        private string pass;
        private string hash;
        public string User { get => user; set => user = value; }
        public string Hash { get => hash; set => hash = value; }
        public string Pass { get => pass; set => pass = value; }
    }
}