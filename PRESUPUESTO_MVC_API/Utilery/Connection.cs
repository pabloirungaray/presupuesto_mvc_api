﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace PRESUPUESTO_MVC_API.Utilery
{
    public class Connection
    {
        private string queryString;
        private SqlConnection connect;

        public Connection(string queryString)
        {
            this.queryString = queryString;
            this.connect = new SqlConnection(queryString);
            this.connect.Open();
            Console.WriteLine("Inicializa objeto");
        }
        public SqlDataReader operate(string query)
        {
            SqlCommand comando = new SqlCommand(query, this.connect);
            SqlDataReader data = comando.ExecuteReader();
            Console.WriteLine("Inicializa objeto");
            return data;
        }
        public void closeConnection()
        {
            this.connect.Close();
        }

    }
}