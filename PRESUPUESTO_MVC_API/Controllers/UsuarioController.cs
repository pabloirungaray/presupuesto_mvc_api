﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using PRESUPUESTO_MVC_API.Models;
using PRESUPUESTO_MVC_API.Utilery;

namespace PRESUPUESTO_MVC_API.Controllers
{
    public class UsuarioController : ApiController
    {
        // GET: api/Usuario
        public IHttpActionResult Get()
        {
            PRESUPUESTO1Entities midb = new PRESUPUESTO1Entities();
            List<USUARIO> usuarios = midb.USUARIOs.ToList();
            return Ok(usuarios);
        }

        // GET: api/Usuario/5
        public IHttpActionResult Get(int id)
        {
            PRESUPUESTO1Entities midb = new PRESUPUESTO1Entities();
            USUARIO usuario = midb.USUARIOs.Find(id);
            return Ok(usuario);
        }

        // POST: api/Usuario
        public IHttpActionResult Post([FromBody] USUARIO usuario)
        {
            PRESUPUESTO1Entities midb = new PRESUPUESTO1Entities();
            Docript enc = new Docript();
            usuario.CLAVE = enc.HexaEncript(usuario.CLAVE);
            midb.USUARIOs.Add(usuario);
            midb.SaveChanges();
            return Ok();
        }

        // PUT: api/Usuario/5

        //[FromBody]  Persona personaUpdate
        public IHttpActionResult Put(int id, [FromBody] USUARIO usuario)
        {
            PRESUPUESTO1Entities midb = new PRESUPUESTO1Entities();
            USUARIO usuario1 =  midb.USUARIOs.Find(id);
            if(usuario1 == null)
            {
                return NotFound();
            }
            Docript enc = new Docript();
            usuario1.CLAVE = enc.HexaEncript(usuario.CLAVE);
            usuario1.CORREO_ELECTRONICO = usuario.CORREO_ELECTRONICO;
            usuario1.FECHA_NACIMIENTO = usuario.FECHA_NACIMIENTO;
            usuario1.NOMBRE_USUARIO = usuario.NOMBRE_USUARIO;
            usuario1.USUARIO1 = usuario1.USUARIO1;
            usuario1.CLAVE = enc.HexaEncript(usuario.CLAVE);
            midb.SaveChanges();
            return Ok();
        }

        // DELETE: api/Usuario/5
        public IHttpActionResult  Delete(int id)
        {
            PRESUPUESTO1Entities midb = new PRESUPUESTO1Entities();
            USUARIO usuario1 = midb.USUARIOs.Find(id);
            if (usuario1 == null)
            {
                return NotFound();

            }
            midb.USUARIOs.Remove(usuario1);
            midb.SaveChanges();            
            return Ok();
        }
    }
}
