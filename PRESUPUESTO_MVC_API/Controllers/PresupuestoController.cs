﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using PRESUPUESTO_MVC_API.Utilery; //@irunga1 -- sqlconnection class
using PRESUPUESTO_MVC_API.Models;
using System.Data.SqlClient;
namespace PRESUPUESTO_MVC_API.Controllers
{
    public class PresupuestoController : ApiController
    {
        // GET: api/Presupuesto
        public IHttpActionResult Get()
        {
            PRESUPUESTO1Entities midb = new PRESUPUESTO1Entities();
            List<PRESUPUESTO> presupuestos = midb.PRESUPUESTOes.ToList();
            return Ok(presupuestos);

            

        }

        // GET: api/Presupuesto/5
        public IHttpActionResult Get(int id)
        {
            PRESUPUESTO1Entities midb = new PRESUPUESTO1Entities();
            PRESUPUESTO presupuesto = midb.PRESUPUESTOes.Find(id);
            return Ok(presupuesto);
        }

        // POST: api/Presupuesto
        public IHttpActionResult Post([FromBody] PRESUPUESTO presupuesto)
        {
            string queryString = "server=LAPTOP-UA2SPSGH ; database=PRESUPUESTO1 ; integrated security = true";
            string query = "select ID_USUARIO from presupuesto WHERE ID_USUARIO = " + presupuesto.ID_USUARIO;
            Connection con = new Connection(queryString);
            SqlDataReader reg = con.operate(query);
            while (reg.Read())
            {
                string dato = reg["ID_USUARIO"].ToString();
                if (dato == presupuesto.ID_USUARIO.ToString())
                {
                    PRESUPUESTO1Entities midb = new PRESUPUESTO1Entities();
                    midb.PRESUPUESTOes.Add(presupuesto);
                    midb.SaveChanges();
                    return Ok("Registro Insertado");
                }
                else
                {
                    return NotFound();
                }
                
            }
            return NotFound();

        }

        // PUT: api/Presupuesto/5
        public IHttpActionResult Put(int id, [FromBody]PRESUPUESTO presupuesto)
        {
            string queryString = "server=LAPTOP-UA2SPSGH ; database=PRESUPUESTO1 ; integrated security = true";
            string query = "select ID_USUARIO from presupuesto WHERE ID_USUARIO = " + presupuesto.ID_USUARIO;
            Connection con = new Connection(queryString);
            SqlDataReader reg = con.operate(query);
            while (reg.Read())
            {
                string dato = reg["ID_USUARIO"].ToString();
                if (dato == presupuesto.ID_USUARIO.ToString())
                {
                    PRESUPUESTO1Entities midb = new PRESUPUESTO1Entities();
                    PRESUPUESTO presupuesto1 = midb.PRESUPUESTOes.Find(id);
                    if (presupuesto1 == null)
                    {
                        return NotFound();
                    }
                    presupuesto1.ID_USUARIO = presupuesto.ID_USUARIO;
                    presupuesto1.MES = presupuesto.MES; 
                    presupuesto1.ANUAL = presupuesto.ANUAL;
                    presupuesto1.MONTO = presupuesto.MONTO;
                    midb.SaveChanges();
                    return Ok("registro actualizado");
                }
                else
                {
                    return NotFound();
                }

            }
            return NotFound();
            //return Ok();
        }

        // DELETE: api/Presupuesto/5
        public IHttpActionResult Delete(int id)
        {
            PRESUPUESTO1Entities midb = new PRESUPUESTO1Entities();
            MOVIMIENTO mov1 = midb.MOVIMIENTOes.Find(id);
            if (mov1 == null)
            {
                return NotFound();

            }
            midb.MOVIMIENTOes.Remove(mov1);            
            midb.SaveChanges();
            return Ok("Registro ELIMINADO");

        }
    }
}
