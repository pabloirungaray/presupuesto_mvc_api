﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using PRESUPUESTO_MVC_API.Utilery; //@irunga1 -- sqlconnection class
using PRESUPUESTO_MVC_API.Models;
using PRESUPUESTO_MVC_API.Class;
using System.Data.SqlClient;

namespace PRESUPUESTO_MVC_API.Controllers
{
    public class GetMovimientosController : ApiController
    {
        [Route("api/GetMovimientos/{Anual}/{Idu}")]
        public IHttpActionResult POST(int Anual, int Idu)
        {
            string queryString = "server=LAPTOP-UA2SPSGH ; database=PRESUPUESTO1 ; integrated security = true";

            string query = "SELECT P.ID_USUARIO IDU, P.ID_PRESUPUESTO, P.MONTO, P.ANUAL, P.MES, M.DESCRIPCION,M.MONTO OPERAR, M.ID_TIPOMOVIMIENTO, M.FECHA FROM MOVIMIENTO M JOIN  PRESUPUESTO P ON P.ID_PRESUPUESTO = M.ID_PRESUPUESTO  WHERE P.ID_USUARIO =" + Idu.ToString() + "AND P.ANUAL =" + Anual.ToString();
            Connection con = new Connection(queryString);
            SqlDataReader reg = con.operate(query);
            List<ListaMovimiento> listaM = new List<ListaMovimiento>();
            int cont = 0;
            while (reg.Read())
            {
                ListaMovimiento itemM = new ListaMovimiento();
                itemM.Id_presupuesto = reg["ID_PRESUPUESTO"].ToString();
                itemM.Monto = (decimal)reg["MONTO"];
                itemM.Anual = reg["ANUAL"].ToString();
                itemM.Descripcion = reg["DESCRIPCION"].ToString();
                itemM.Mes = reg["MES"].ToString();
                itemM.Operar = (decimal)reg["OPERAR"];
                itemM.Tipomovimiento = (int)reg["ID_TIPOMOVIMIENTO"];
                itemM.Fecha = reg["FECHA"].ToString();
                itemM.Idu = (int)reg["IDU"];
                listaM.Add(itemM);
                cont++;
            }
            if (cont > 0)
            {
                return Ok(listaM);
            }
            else
            {
                return NotFound();
            }


        }

        // PUT: api/GetMovimientos/5


        // DELETE: api/GetMovimientos/5

    }
}
