﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using PRESUPUESTO_MVC_API.Utilery; //@irunga1 -- sqlconnection class
using PRESUPUESTO_MVC_API.Models;
using PRESUPUESTO_MVC_API.Class;
using System.Data.SqlClient;
using System.Web.Http;

namespace PRESUPUESTO_MVC_API.Controllers
{
    public class MovimientosController : ApiController
    {
        // GET: api/Movimientos
        public IHttpActionResult Get()
        {
            PRESUPUESTO1Entities midb = new PRESUPUESTO1Entities();
            List<MOVIMIENTO> movimientos = midb.MOVIMIENTOes.ToList();
            return Ok(movimientos);
        }

        // GET: api/Movimientos/5
        public IHttpActionResult Get(int id)
        {
            PRESUPUESTO1Entities midb = new PRESUPUESTO1Entities();
            MOVIMIENTO movimiento = midb.MOVIMIENTOes.Find(id);
            return Ok(movimiento);
        }

        // POST: api/Movimientos
        public IHttpActionResult Post([FromBody] MOVIMIENTO movimiento)
        {
            string queryString = "server=LAPTOP-UA2SPSGH ; database=PRESUPUESTO1 ; integrated security = true";
            string query = "select * from MOVIMIENTO where ID_PRESUPUESTO = " + movimiento.ID_PRESUPUESTO;
            Connection con = new Connection(queryString);
            SqlDataReader reg = con.operate(query);
            while (reg.Read())
            {
                string dato = reg["ID_PRESUPUESTO"].ToString();
                if (dato == movimiento.ID_PRESUPUESTO.ToString())
                {
                    PRESUPUESTO1Entities midb = new PRESUPUESTO1Entities();
                    midb.MOVIMIENTOes.Add(movimiento);
                    midb.SaveChanges();
                    return Ok("Registro Insertado");
                }
                else
                {
                    return NotFound();
                }

            }
            return NotFound();
            
        }

        // PUT: api/Movimientos/5
        public IHttpActionResult Put(int id, [FromBody]MOVIMIENTO movimiento)
        {
            string queryString = "server=LAPTOP-UA2SPSGH ; database=PRESUPUESTO1 ; integrated security = true";
            string query = "select * from MOVIMIENTO where ID_PRESUPUESTO = " + movimiento.ID_PRESUPUESTO;
            Connection con = new Connection(queryString);
            SqlDataReader reg = con.operate(query);
            while (reg.Read())
            {
                string dato = reg["ID_PRESUPUESTO"].ToString();
                if (dato == movimiento.ID_PRESUPUESTO.ToString())
                {
                    PRESUPUESTO1Entities midb = new PRESUPUESTO1Entities();
                    MOVIMIENTO movimiento1 = midb.MOVIMIENTOes.Find(id);
                    if (movimiento1 == null)
                    {
                        return NotFound();
                    }


                    movimiento1.ID_PRESUPUESTO = movimiento.ID_PRESUPUESTO;
                    movimiento1.MONTO = movimiento.MONTO;
                    movimiento1.DESCRIPCION = movimiento.DESCRIPCION;
                    movimiento1.FECHA = movimiento.FECHA;
                    midb.SaveChanges();
                    return Ok("registro actualizado");
                }
                else
                {
                    return NotFound();
                }

            }
            return NotFound();
            
        }

        // DELETE: api/Movimientos/5
        public IHttpActionResult Delete(int id)
        {
            PRESUPUESTO1Entities midb = new PRESUPUESTO1Entities();
            MOVIMIENTO movimiento1 = midb.MOVIMIENTOes.Find(id);
            if (movimiento1 == null)
            {
                return NotFound();

            }
            midb.MOVIMIENTOes.Remove(movimiento1);
            midb.SaveChanges();
            return Ok("Registro ELIMINADO");
        }
    }
}
